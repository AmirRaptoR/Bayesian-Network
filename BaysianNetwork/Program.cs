﻿using System;
using System.IO;
using System.Linq;

namespace BaysianNetwork
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var lines = File.ReadAllLines("data.csv");
            int[][] data = new int[lines.Length - 1][];
            for (int i = 1; i < lines.Length; i++)
            {
                var splitted = lines[i].Split(new[] {',', ' '}, StringSplitOptions.RemoveEmptyEntries);
                var tmp = new int[splitted.Length - 1];

                for (int j = 0; j < tmp.Length; j++)
                    tmp[j] = int.Parse(splitted[j + 1]);
                data[i - 1] = tmp.Reverse().ToArray();
            }

            var network = new BaysianNetwork<int>();
            var node = network.LearnNetwork(data);

            Console.ReadKey();
        }
    }
}