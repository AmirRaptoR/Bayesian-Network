﻿using System.Collections.Generic;

namespace BaysianNetwork
{
    public class Node<T>
    {
        public int Index { get; set; }
        public List<Node<T>> Parents { get; set; } = new List<Node<T>>();


    }
}