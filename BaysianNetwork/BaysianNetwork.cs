using System;
using System.Collections.Generic;
using System.Linq;

namespace BaysianNetwork
{
    public class BaysianNetwork<T>
    {
        private T[][] _allPosibleValues;
        private T[][] _data;

        private int[] _r;

        public IEnumerable<Node<T>> LearnNetwork(T[][] data)
        {
            _data = data;
            // number of nodes
            var n = data[0].Length;
            // upper bound for number of parent any node can have
            var u = n - 1;

            // create all n nodes
            var nodes = Enumerable.Range(0, n).Select((x, i) => new Node<T> { Index = i }).ToList();

            // all possible values
            _allPosibleValues =
                Enumerable.Range(0, n).Select(index => data.Select(x => x[index]).Distinct().ToArray()).ToArray();
            _r = _allPosibleValues.Select(x => x.Length).ToArray();

            for (var i = 0; i < n; i++)
            {
                var node = nodes[i];

                var pOld = CalculateK2Score(node, i);
                var okToProceed = true;
                while (okToProceed && node.Parents.Count < u)
                {
                    var ind = i;
                    var z = GetBestForParent(node, nodes.Where((x, index) => index < ind && !node.Parents.Contains(x)).ToList(), i);
                    if (z == null)
                        break;
                    node.Parents.Add(z);
                    var pNew = CalculateK2Score(node, i);

                    if (pNew > pOld)
                    {
                        pOld = pNew;
                    }
                    else
                    {
                        node.Parents.Remove(z);
                        okToProceed = false;
                    }
                }
                Console.WriteLine($"Node {node.Index} parents are {string.Join(", ", node.Parents.Select(x => x.Index))}");
            }
            return nodes;
        }

        private Node<T> GetBestForParent(Node<T> node, IList<Node<T>> posibleParents, int i)
        {
            var max = double.MinValue;
            Node<T> best = null;
            foreach (var posibleParent in posibleParents)
            {
                node.Parents.Add(posibleParent);
                var f = CalculateK2Score(node, i);
                node.Parents.Remove(posibleParent);
                if (!(f > max)) continue;
                max = f;
                best = posibleParent;
            }
            return best;
        }

        private double Factorial(int value)
        {
            var m = 1;
            for (var i = 2; i <= value; i++)
                m *= i;
            return m;
        }

        private double CalculateK2Score(Node<T> node, int i)
        {
            var parentsIndex = node.Parents.Select(x => x.Index).ToArray();
            var m = 1.0;
            if (node.Parents.Count == 0)
            {
                m *= Factorial(_r[i] - 1) / Factorial(N(i, -1, null, null) + _r[i] - 1);
                for (var k = 0; k < _r[i]; k++)
                    m *= Factorial(Alpha(i, -1, k, null, null));
            }
            else
            {
                var tmp = new T[node.Parents.Count];
                var result = new List<T[]>();
                GetAllPosibleParent(parentsIndex, tmp, 0, result);
                for (var j = 0; j < result.Count; j++)
                {
                    m *= Factorial(_r[i] - 1) / Factorial(N(i, j, parentsIndex, result[j]) + _r[i] - 1);
                    for (var k = 0; k < _r[i]; k++)
                        m *= Factorial(Alpha(i, j, k, parentsIndex, result[j]));
                }
            }
            return m;
        }

        private void GetAllPosibleParent(IList<int> nodes, T[] value, int index, List<T[]> result)
        {
            foreach (var posibleVars in _allPosibleValues[nodes[index]])
            {
                value[index] = posibleVars;
                if (index < value.Length - 1)
                    GetAllPosibleParent(nodes, value, index + 1, result);
                else
                    result.Add(value.Clone() as T[]);
            }
        }


        private int Alpha(int i, int j, int k, int[] parents, T[] values)
        {
            if (j == -1)
            {
                return _data.Count(x => x[i].Equals(_allPosibleValues[i][k]));
            }
            var count = 0;

            foreach (var instance in _data)
            {
                if (instance[i].Equals(_allPosibleValues[i][k]))
                {
                    bool isEqual = true;
                    for (int l = 0; l < parents.Length; l++)
                        if (!values[l].Equals(instance[parents[l]]))
                        {
                            isEqual = false;
                            break;
                        }
                    if (isEqual)
                        count++;
                }
            }
            return count;
        }


        private int N(int i, int j, int[] parents, T[] values)
        {
            if (j == -1)
                return Enumerable.Range(0, _r[i]).Sum(k => Alpha(i, j, k, parents, values));
            var s = 0;
            for (var k = 0; k < _r[i]; k++)
                s += Alpha(i, j, k, parents, values);
            return s;
        }
    }
}